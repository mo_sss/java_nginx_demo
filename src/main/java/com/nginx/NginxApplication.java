package com.nginx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @func: Niginx部署项目测试启动类
 * @author: LiBai
 * @version: v1.0
 * @createTime: 2024/12/4 9:41
 */
@SpringBootApplication
public class NginxApplication {
    public static void main(String[] args) {
        SpringApplication.run(NginxApplication.class, args);
    }
}
