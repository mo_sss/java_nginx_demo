package com.nginx.test;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @func: 测试接口
 * @author: LiBai
 * @version: v1.0
 * @createTime: 2024/12/4 9:42
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @GetMapping(value = "/get")
    public JSONObject test(){
        System.out.println("接口测试调用成功");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("test", "接口测试返回结果");
        return jsonObject;

    }

}
